<?php

namespace App\Http\Middleware;

use App\Http\Helpers\LangHelper;
use TCG\Voyager\Facades\Voyager;
use App\Product;
use App\Product_category;
use Closure;

class SharaAbleData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->getShareData();
        return $next($request);
    }

    public function getShareData(){
        $products = Product::with('translations')
        ->with('product_category')
        ->with('product_colors')
        ->get()->toArray() ;
        $products = LangHelper::translate($products,\App::getLocale());

        $categories_menu = Product_category::with('translations')->where('parent_id',0)->with('children')->where('status',1)->limit(4)->get()->toArray();
        $categories_menu = LangHelper::translate($categories_menu,\App::getLocale());
        
        $settings_en = array(
            "title" => setting('site.title'),
            "description" => setting('site.description'),
            "meta_keywords" => setting('site.meta_keywords') 
        );
        $settings_ar = array(
            "title" => setting('site.title_ar'),
            "description" => setting('site.description_ar'),
            "meta_keywords" => setting('site.meta_keywords_ar') 
        );

        if(\App::getLocale() == 'ar'){
            $settings = $settings_ar;
        }else{
            $settings = $settings_en;
        }
        //dd(setting('site.title')); 
        \View::share(compact('products','categories_menu'));
        //dd($products) ;
         //return \App::getLocale();
    }
}
