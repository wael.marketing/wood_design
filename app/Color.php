<?php

namespace App;

use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    use Translatable;
    protected $table = "colors";
    protected $translatable = ['title'];
}
