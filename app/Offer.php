<?php

namespace App;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;


use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use Translatable;
    protected $table = "offers";
    protected $translatable = ['Title','body'];

}
