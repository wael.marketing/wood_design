<?php 

return [
    'welcome' => 'مرحبا بكم',
    'my_account' => 'حسابى',
    'categories' => 'الأقسام',
    'search' => 'بحث' , 
    'login' => 'الدخول' ,
    'sign_up' => 'التسجيل',
    /** Menu */
    'home' => 'الرئيسية',
    'about_us' => 'من نحن؟',
    'products' => 'المنتجات',
    'blog' => "المدونة",
    'contact' => 'اتصل بنا',
    /** cart */
    'cart' => 'سلة الشراء',
    'add_to_cart' => 'ضف الى السله',
    'remove_from_cart' => "إزالة",
    'off' => 'خصم' , 
    /** News letter */
    'subscribe' => 'إشترك',
    'email' => 'البريد الالكترونى',
    'sing_up_news' => "إشترك فى النشره الاخبارية",
    'get_latest' => "احصل على اخر العروض والخصومات"

];
