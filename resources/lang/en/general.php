<?php

return [
    'welcome' => 'Welcome to MyStore!',
    'my_account' => 'My Account',
    'categories' => 'categories',
    'search' => 'search' , 
    'login' => 'Login' ,
    'sign_up' => 'Sign Up',
    /** Menu */
    'home' => 'Home',
    'about_us' => 'About Us',
    'products' => 'Products',
    'blog' => "Blog",
    'contact' => 'contact',
    /** cart */
    'cart' => 'Shopping Cart',
    'add_to_cart' => 'Add To Cart',
    'remove_from_cart' => "Remove",
    'off' => 'off' , 
    /** News letter */
    'subscribe' => 'Subscribe',
    'email' => 'Email address',
    'sing_up_news' => "SIGN UP FOR NEWSLETTER",
    'get_latest' => "Get the latest deals and special offers"

];